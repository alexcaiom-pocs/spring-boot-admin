/**
 * 
 */
package br.com.massudaalexander.password.validation.validator;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import br.com.massudaalexander.password.validation.dto.PasswordValidateRequestDTO;
import br.com.massudaalexander.password.validation.validator.chain.PasswordValidation;

/**
 * @author Alex
 *
 */
@Component
public class RepitedCharacterValidation implements PasswordValidation {
	
	private static final String MESSAGE = "Não possuir caracteres repetidos dentro do conjunto";
	private PasswordValidation next;

	@Override
	public void validate(PasswordValidateRequestDTO request, List<String> messages) {
		doValidate(request, messages);
		
		if (CollectionUtils.isEmpty(messages) && Objects.nonNull(next)) {
			next.validate(request, messages);
		}
	}

	private void doValidate(PasswordValidateRequestDTO request, List<String> messages) {
		
		if (StringUtils.isEmpty(request.getPassword()) || hasRepitedCharacter(request.getPassword())) {
			messages.add("Digite uma senha com " + MESSAGE);
		}
	}

	private boolean hasRepitedCharacter(String password) {
		
		char[] chars = password.toCharArray();
		
		Map<Character, Integer> map = new HashMap<>();
		for (char c : chars) {
			if (map.containsKey(c)) {
				return true;
			} else {
				map.put(c, 1);
			}
		}
		
		return false;
	}

	@Override
	public void setNext(PasswordValidation validation) {
		this.next = validation;
	}

}
