/**
 * 
 */
package br.com.massudaalexander.password.validation.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.google.gson.Gson;

import br.com.massudaalexander.password.validation.dto.PasswordValidateRequestDTO;
import br.com.massudaalexander.password.validation.service.PasswordValidatorServiceImpl;

/**
 * @author Alex
 *
 */
public class PasswordControllerTest {
	
	@InjectMocks PasswordController controller;
	@Mock private PasswordValidatorServiceImpl service;
	
	MockMvc mockMvc;
	
	@BeforeEach
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
	}

	/**
	 * Test method for {@link br.com.massudaalexander.password.validation.controller.PasswordController#validate(br.com.massudaalexander.password.validation.dto.PasswordValidateRequestDTO)}.
	 * @throws Exception 
	 */
	@Test
	public void testValidate() throws Exception {
		PasswordValidateRequestDTO request = new PasswordValidateRequestDTO();
		request.setPassword("lifesgood");
		mockMvc.perform(
				post("/password/validate")
				.contentType(MediaType.APPLICATION_JSON)
				.content(new Gson().toJson(request))
				)
		.andDo(print())
		.andExpect(status().isOk());
	}

}
