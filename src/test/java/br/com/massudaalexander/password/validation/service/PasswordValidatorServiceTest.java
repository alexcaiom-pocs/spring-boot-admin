package br.com.massudaalexander.password.validation.service;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.util.CollectionUtils;

import br.com.massudaalexander.password.validation.CommonValues;
import br.com.massudaalexander.password.validation.dto.PasswordValidateRequestDTO;
import br.com.massudaalexander.password.validation.dto.PasswordValidateResponseDTO;
import br.com.massudaalexander.password.validation.validator.chain.PasswordValidationChain;

class PasswordValidatorServiceTest {
	
	@InjectMocks private PasswordValidatorServiceImpl service;
	@Mock private PasswordValidationChain chain;

	@BeforeEach
	void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	void testValidate() {
		PasswordValidateRequestDTO request = CommonValues.getPasswordValidateRequestDTO("password");
		PasswordValidateResponseDTO response = service.validate(request);
		assertNotNull(response, "Should not be null");
		assertTrue(CollectionUtils.isEmpty(response.getMessages()), "Should be empty");
	}

}


